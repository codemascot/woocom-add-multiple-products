<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://sodathemes.com
 * @since      1.0.0
 *
 * @package    Woocom_Add_Multiple_Products
 * @subpackage Woocom_Add_Multiple_Products/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->